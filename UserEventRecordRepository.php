<?php

namespace App\Repositories;

use App\User;
use App\UserBag;
use App\UserEventRecord;

class UserEventRecordRepository
{
    private function isIpSuspectForUser($ip, User $user)
    {
        return $user->country_code !== geoip($ip)->iso_code;
    }

    public function createSignIn($ip, $isSuccess, User $user)
    {
        $geo = geoip($ip);
        $record = new UserEventRecord([
            'user_id' => $user->id,
            'type'    => UserEventRecord::TYPES['SIGN_IN'],
            'data'    => [
                'ip'           => $ip,
                'is_success'   => $isSuccess,
                'is_suspect'   => $this->isIpSuspectForUser($ip, $user),
                'country_code' => $geo->iso_code,
                'location'     => $this->getUserLocation($geo)
            ]
        ]);

        $record->save();

        return $record;
    }

    /**
     * Get user's country and city if exist
     * for sign in description
     *
     * @param $geo
     * @return string
     */
    private function getUserLocation($geo)
    {
        if (!$geo->default and (!empty($geo->country) or !empty($geo->city))){
            $location[] = $geo->country;
            $location[] = $geo->city;
            return implode(', ', $location);
        }
        return '';
    }

    public function createBagsProfit($amount, User $user)
    {
        $record = new UserEventRecord([
            'user_id' => $user->id,
            'type'    => UserEventRecord::TYPES['BAGS_PROFIT'],
            'data'    => [
                'amount' => $amount
            ]
        ]);

        $record->save();

        return $record;
    }

    public function createGiftBagProfit($amount, User $user)
    {
        $record = new UserEventRecord([
            'user_id' => $user->id,
            'type'    => UserEventRecord::TYPES['GIFT_BAG_PROFIT'],
            'data'    => [
                'amount' => $amount
            ]
        ]);

        $record->save();

        return $record;
    }

    public function createReferralsProfit($amount, $userId)
    {
        $record = new UserEventRecord([
            'user_id' => $userId,
            'type'    => UserEventRecord::TYPES['REFERRALS_PROFIT'],
            'data'    => [
                'amount' => $amount
            ]
        ]);

        $record->save();

        return $record;
    }

    public function createBagActivated(UserBag $userBag)
    {
        $record = new UserEventRecord([
            'user_id' => $userBag->user_id,
            'type'    => UserEventRecord::TYPES['BAG_ACTIVATED'],
            'data'    => [
                'bag_id'      => $userBag->bag_id,
                'user_bag_id' => $userBag->id
            ]
        ]);

        $record->save();

        return $record;
    }

    public function createBagBuy(UserBag $userBag)
    {
        $record = new UserEventRecord([
            'user_id' => $userBag->user_id,
            'type'    => UserEventRecord::TYPES['BAG_BUY'],
            'data'    => [
                'user_id'     => $userBag->user_id,
                'user_bag_id' => $userBag->id,
                'bag_id'      => $userBag->bag_id
            ]
        ]);

        $record->save();

        return $record;
    }

    /**
     * Create bag sold user event
     *
     * @param UserBag $userBag
     * @return UserEventRecord
     */
    public function createBagSold(UserBag $userBag)
    {
        $record = new UserEventRecord([
            'user_id' => $userBag->user_id,
            'type'    => UserEventRecord::TYPES['BAG_SOLD'],
            'data'    => [
                'user_id'     => $userBag->user_id,
                'user_bag_id' => $userBag->id,
                'bag_id'      => $userBag->bag_id
            ]
        ]);

        $record->save();

        return $record;
    }

    public function createNewMessage(User $user, $idDialog, $fromUserId = false)
    {
        $record = new UserEventRecord([
            'user_id' => $user->id,
            'type'    => UserEventRecord::TYPES['NEW_MESSAGE'],
            'data'    => [
                'user_id'   => $fromUserId,
                'dialog_id' => $idDialog
            ]
        ]);

        $record->save();

        return $record;
    }

    public function createNewReferral(User $user, User $fromUser)
    {
        $record = new UserEventRecord([
            'user_id' => $user->id,
            'type'    => UserEventRecord::TYPES['NEW_REFERRAL'],
            'data'    => [
                'user_id' => $fromUser->id
            ]
        ]);

        $record->save();

        return $record;
    }

    public function createBonusReferrals(User $user, $bonusData)
    {
        $record = new UserEventRecord([
            'user_id' => $user->id,
            'type'    => UserEventRecord::TYPES['BONUS_REFERRALS'],
            'data'    => [
                'amount'        => $bonusData['amount'],
                'achievement'   => $bonusData['achievement']
            ]
        ]);

        $record->save();

        return $record;
    }

    public function createBonusLevel(User $user, $bonusData)
    {
        $record = new UserEventRecord([
            'user_id' => $user->id,
            'type'    => UserEventRecord::TYPES['BONUS_LEVEL'],
            'data'    => [
                'amount'        => $bonusData['amount'],
                'achievement'   => $bonusData['achievement']
            ]
        ]);

        $record->save();

        return $record;
    }
}