import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Link from 'react-router-dom/Link';
import styles from './games.scss';

import MultiSelect from '../../components/MultiSelect';
import Item from '../../components/Item/Item';
import Pagination from '../Pagination/';

function getCurrentPage(offset, limit) {
    return (offset > 0 ? Math.ceil(offset / limit) : 0) + 1;
}

function getTotalPages(total, limit) {
    return Math.ceil(total / limit);
}

function prepareItemForSelect(item) {
    return {
        id: item.id,
        label: item.name
    };
}

class Games extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            page: getCurrentPage(props.query.offset, props.query.limit),
            totalPages: getTotalPages(props.result.total, props.query.limit),
            genres: props.genres.map(prepareItemForSelect),
            languages: props.languages.map(prepareItemForSelect),
            operationSystems: props.operationSystems.map(prepareItemForSelect)
        };
    }

    componentWillReceiveProps(props) {
        this.setState({
            page: getCurrentPage(props.query.offset, props.query.limit),
            totalPages: getTotalPages(props.result.total, props.query.limit)
        });
    }

    onSelectChangeHandler = (value, name) => {
        let query = this.props.query;

        query = { ...query, filters: { ...query.filters, [name]: value } };

        this.fetchData(query);
    };

    fetchData(query) {
        this.props.onChange(query);
    }

    render() {
        const { list } = this.props.result;
        const { query } = this.props;

        const itemList = list.map(itm => {
            return <div className={styles.item} key={itm.id}><Item item={itm} /></div>;
        });

        return (
            <div className="games">
                <div className={styles.inner}>
                    <aside className={styles.sidebar}>
                        <div className={styles.filter}>
                            <div className={styles.filter__block}>
                                <div className={styles.filter__title}>Genre:</div>
                                <div className={styles.filter__select}>
                                    <MultiSelect
                                        items={this.state.genres}
                                        name="genres"
                                        value={query.filters.genres || []}
                                        onChange={this.onSelectChangeHandler}
                                    />
                                </div>
                            </div>
                            <div className={styles.filter__block}>
                                <div className={styles.filter__title}>Language:</div>
                                <div className={styles.filter__select}>
                                    <MultiSelect
                                        items={this.state.languages}
                                        name="languages"
                                        value={query.filters.languages || []}
                                        onChange={this.onSelectChangeHandler}
                                    />
                                </div>
                            </div>
                            <div className={styles.filter__block}>
                                <div className={styles.filter__title}>OS:</div>
                                <div className={styles.filter__select}>
                                    <MultiSelect
                                        items={this.state.operationSystems}
                                        name="operationSystems"
                                        value={query.filters.operationSystems || []}
                                        onChange={this.onSelectChangeHandler}
                                    />
                                </div>
                            </div>
                        </div>
                    </aside>
                    <div className={styles.content}>
                        {/*<div className={styles.sort}>*/}
                        {/*<span className={styles.sort__label}>Sort by:</span>*/}
                        {/*<Link className={styles.sort__link} to="/">Release Date</Link>*/}
                        {/*</div>*/}

                        <div className={styles.items}>
                            {itemList}
                        </div>

                        {this.state.totalPages > 1 &&
                            <div className={styles.pagination}>
                                <Pagination page={this.state.page} total={this.state.totalPages} />
                            </div>
                        }
                    </div>
                </div>
            </div>
        );
    }
}

Games.propTypes = {
    query: PropTypes.shape({
        offset: PropTypes.number,
        limit: PropTypes.number,
        filters: PropTypes.object
    }).isRequired,

    result: PropTypes.shape({
        total: PropTypes.number,
        list: PropTypes.array
    }).isRequired,

    onChange: PropTypes.func.isRequired
};

function mapStateToProps(state) {
    return {
        genres: state.app.genres,
        languages: state.app.languages,
        operationSystems: state.app.operationSystems
    };
}

export default connect(mapStateToProps)(Games);