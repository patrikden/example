import express from 'express';
import {param as paramCheck} from 'express-validator/check';
import {sanitizeParam} from 'express-validator/filter';
import {processForm} from "../utils/FormHandler";
import TransactionService from "../services/TransactionService";
import GameService from "../services/GameService";
import URLService from "../services/URLService";
import PaymentGatewayService from "../services/PaymentGatewayService";
import FriendshipService from "../services/FriendshipService";

function formatTransactionForUI(tx) {
  const purchase = tx.related('purchase');
  const bot = purchase.related('bot');
  const pack = purchase.get('package');

  return {
    id: tx.id,
    package: {
      id: pack.id,
      game_id: pack.games[0].id,
      thumbnail: pack.games[0].data.header_image,
      name: pack.name,

      price: {
        final: tx.get('amount'),
        currency: tx.get('currency'),
      },
    },

    amount: tx.get('amount'),
    currency: tx.get('currency'),
    payment_status: tx.get('status'),
    purchase_status: purchase.get('status'),
    created_at: tx.get('created_at'),
    bot: bot
      ? {
        name: bot.get('name')
      }
      : null
  };
}

function getTransactionIdSanitation() {
  return sanitizeParam('id').toInt(10);
}

function getTransactionIdValidation() {
  return paramCheck('id', 'Invalid id field.')
    .exists()
    .custom((id, {req}) => {
      return TransactionService.getTransactionByIdForUser(id, req.user.id)
        .then(tx => req.transaction = tx);
    });
}

export default function TxModule() {
  const expressRouter = express.Router();

  expressRouter.get('/all', function (req, res) {
    processForm(req, res, function () {
      return TransactionService.getTransactionsForUser(req.user.id)
        .then(transactions => {
          return {transactions: transactions.map(formatTransactionForUI)};
        });
    });
  });

  expressRouter.get('/:id', [
    getTransactionIdSanitation(),
    getTransactionIdValidation()
  ], function (req, res) {
    processForm(req, res, function () {
      return {transaction: formatTransactionForUI(req.transaction)};
    });
  });

  expressRouter.post('/checkout/:packageId', [
    paramCheck('packageId', 'Invalid packageId field.')
      .exists()
      .custom((packageId, {req}) => {
        return GameService.getPackageById(packageId).then(pack => req.package = pack);
      })
  ], function (req, res) {
    processForm(req, res, function () {
      return PaymentGatewayService.makePayment(req.user.id, req.package)
        .then(transaction => {
          return {pay_url: URLService.generatePaymentURL(transaction.get('gateway_token'))};
        });
    });
  });

  expressRouter.get('/friend/:id', [
    getTransactionIdSanitation(),
    getTransactionIdValidation()
  ], function (req, res) {
    processForm(req, res, function () {
      const currency = req.transaction.related('purchase').get('meta').base.currency;

      return Promise.all([
        FriendshipService.checkInFriends(req.user.get('steam_id'), currency),
        FriendshipService.getPendingFriendRequest(req.user.get('steam_id'), currency)
      ])
        .then(([checkRequest, pendingRequest]) => ({
          isFriend: checkRequest && checkRequest.get('is_friend'),
          hasPendingRequest: !!pendingRequest
        }));
    });
  });

  expressRouter.post('/friend/:id', [
    getTransactionIdSanitation(),
    getTransactionIdValidation()
  ], function (req, res) {
    processForm(req, res, function () {
      const currency = req.transaction.related('purchase').get('meta').base.currency;

      return FriendshipService.sendFriendRequest(req.user.id, req.user.get('steam_id'), currency)
        .then(() => null);
    });
  });

  return expressRouter;
}