<?php

namespace App\Http\Controllers;

use App\FinanceAccount;
use App\FinanceSystem;
use App\Http\Requests\Profile\StoreFinanceAccount;
use App\Http\Requests\Profile\StoreProfile;
use App\Services\UserService;
use Illuminate\Http\Request;

class ProfileController extends Controller
{

    public function index()
    {
        $user = $this->getGuardUser();
        $user->load(['financeAccounts']);
        $financeSystems = FinanceSystem::ordered()->get();

        return view('profile.index', ['user' => $user, 'financeSystems' => $financeSystems]);
    }

    public function saveProfile(StoreProfile $request)
    {
        $user = $this->getGuardUser();

        if ($request->hasFile('photo')) {
            $path = $request->file('photo')->store('avatars', 'public');
            $user->avatar = $path;
        }

        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');

        $user->save();

        $actions = array_merge(
            $this->actionPopup('profile-save-success'),
            $this->actionReload()
        );

        return $this->jsonActions($actions);
    }

    public function storeFinanceAccount(StoreFinanceAccount $request, UserService $userService)
    {
        $user = $this->getGuardUser();

        if (!$userService->checkUserPaymentPassword($user, $request->input('payment_password'))) {
            return $this->jsonErrors(['payment_password' => 'Неверный платёжный пароль.']);
        }

        $financeAccount = $user->getFinanceAccountByType($request->input('finance_system_id'));

        if(!$financeAccount) {
            $financeAccount = new FinanceAccount();
            $financeAccount->finance_system_id = $request->input('finance_system_id');
            $financeAccount->currency = $request->input('currency');
            $financeAccount->user()->associate($user);
        }

        $financeAccount->data = $request->input('data');
        $financeAccount->save();

        $actions = array_merge(
            $this->actionPopup('profile-save-success'),
            $this->actionReload()
        );

        return $this->jsonActions($actions);
    }
}
