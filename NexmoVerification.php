<?php

namespace App\SMS\Nexmo;

use Nexmo\Verify\Verification;

/**
 * Class NexmoVerification
 * @package App\SMS\Nexmo
 */

class NexmoVerification extends NexmoBase
{

    const FAKE_VALID_VERIFICATION_CODE = '0000';
    const RESEND_TIMEOUT = 60;
    const PIN_EXPIRY = 300;
    const STATUSES = [
        'IN_PROGRESS' => 'IN PROGRESS',
        'SUCCESSFUL'  => 'SUCCESSFUL',
        'FAILED'      => 'FAILED',
        'EXPIRED'     => 'EXPIRED',
        'CANCELLED'   => 'CANCELLED',
        'INVALID'     => '101'
    ];

    /**
     * Init SMS verification process
     *
     * @param   $phone  integer     User's phone number
     * @return  null|string
     */
    public function start($phone)
    {
        $request = $this->client->verify()->start([
            'number'          => $this->cleanNumber($phone),
            'brand'           => $this->config->get('nexmo.brand'),
            'pin_expiry'      => self::PIN_EXPIRY,
            'next_event_wait' => self::RESEND_TIMEOUT
        ]);

        return $request->getRequestId();
    }

    /**
     * Cancel verification process fo requestId
     *
     * @param $requestId    string  RequestId
     * @return bool|void
     */
    public function cancel($requestId)
    {
        $verification = $this->getVerification($requestId);

        if ($this->isVerificationInProgress($verification)) {
            return $verification->cancel();
        }

        return false;
    }

    /**
     * Check verification by pair requestId and code
     *
     * @param $requestId    string  RequestId
     * @param $code         string  SMS code
     * @return bool
     */
    public function check($requestId, $code)
    {
        $verification = $this->getVerification($requestId);

        if ($this->isVerificationInProgress($verification)) {
            return $verification->check($code);

        }

        return false;
    }

    /**
     * Resend Verification instead of requestId
     *
     * @param $requestId    string  RequestId
     * @return bool|null|string
     */
    public function resend($requestId)
    {
        $verification = $this->getVerification($requestId);

        if ($this->isVerificationInProgress($verification) or $verification->getStatus() == self::STATUSES['FAILED']) {
            $this->cancel($requestId);
            return $this->start($verification->getNumber());

        } else {
            return false;
        }
    }

    /**
     * Get verification by requestId
     *
     * @param $requestId    string  RequestId
     * @return Verification
     */
    private function getVerification($requestId)
    {
        return $this->client->verify()->search($requestId);
    }

    /**
     * Check verification progress status
     *
     * @param Verification $verification    verification object
     * @return bool
     */
    private function isVerificationInProgress(Verification $verification)
    {
        return $verification->getStatus() == self::STATUSES['IN_PROGRESS'];
    }

    /**
     * Get RESEND_TIMEOUT const
     *
     * @return int
     */
    public function getResendTimeout()
    {
        return self::RESEND_TIMEOUT;
    }
}